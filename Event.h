#pragma once
#include <functional>
#include "event_base.h"
#include "Application.h"

template <typename... Args>
void Event<Args...>::Invoke(Args... args)
{
    std::function<void()> fn = [=]
    {
        this->act.Call(args...);
    };
    Application::Enqueue(fn);
}

//typedef Event<> Event;
