#include "Application.h"
#include <iostream>
#include "Action.h"
#include "Event.h"
#include <thread>
#include <unistd.h>
using namespace std;

class Tester
{

public:
    Event ev; //this doesnt work
    Tester() {
        ev.Connect(this, &Tester::on_ev);
        ev.Invoke();
    }

    void on_ev() {
        cout << "Event\n";
        Application::Quit();
    }
};

void on_quit()
{
    cout << "Quitting\n";
}

int main(int argc, char const *argv[])
{
    Application app;
    Application::onQuit.Connect(on_quit);
    Tester t;
    app.Run();
    return 0;
}
