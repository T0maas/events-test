#pragma once
#include <functional>

template <typename... Args>
class Action
{
private:
    std::function<void(Args...)> fn;

public:
    Action()
    {
        fn = [](Args...) {};
    }
    Action(void (*fnp)(Args...))
    {
        this->fn = fnp;
    }
    template <typename _Object>
    Action(_Object *_class, void (_Object::*_method)(Args...))
    {
        auto l = [_class, _method](Args... args)
        {
            (_class->*_method)(args...);
        };
        this->fn = l;
    }
    void Call(Args... args)
    {
        this->fn(args...);
    }
};
