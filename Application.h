#pragma once
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>
#include "event_base.h"

class Application
{
private:
    static Application *app;
    std::queue<std::function<void()>> myq;
    std::mutex m_cv, m_q;
    std::condition_variable cv;
    bool running;

public:
    Application();
    static void Enqueue(std::function<void()> fn);
    static void Quit();
    static Event<> onQuit;
    void Run() &;
};
