SRC = $(wildcard *.cc)
OBJ = $(SRC:.cc=.o)
CPPFLAGS = -std=c++17 -c -O2 
LDFLAGS = -lpthread

all: main



%.o: %.cc
	g++ $(CPPFLAGS) $< -o $@

main: $(OBJ)
	g++ $^ $(LDFLAGS) -o $@


depend: .depend

.depend: $(SRC)
	rm -f "$@"
	g++ $(CPPFLAGS) -MM $^ > "$@"

include .depend



clean:
	rm -rf *.o
